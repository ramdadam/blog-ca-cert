# Introduction
I have worked in some corporate environments that implemented their own [Root Certificate Authority (CA)](https://en.wikipedia.org/wiki/Root_certificate). It's important to note that Root CAs hold the highest level of trust and are pre-installed in various software, such as operating systems and web browsers. Therefore, establishing a custom Root CA can be reasonable when you have complete control over the clients. However, I have observed that teams tend to disable certificate verification checks rather than installing the CA's certificate, which poses a security risk.

# Chain of Trust
Establishing a trusted connection in an untrusted environment is an interesting challenge. This is something that has to be done everytime a client connects to a server over a secured channel like HTTPS. Usually it will serve a certificate that the client will run a verification process, which consists of multiple checks. One of those checks is to verify if the client can trust the served certificate. This is done by traversing the chain of certificates that were used to sign the transmitted certificate.

![cert_viewer.png](img/cert_viewer.png "Certificate Viewer for google certificate")

You can see that there is a hierarchy behind a certificate. Each level is signed by the level above it. The topmost certificate (1) is the root certificate. This is usually pre-installed on your system (your trust store) or is somehow added afterward. Between the root certificate (1) and the server certificate (3) are one or more intermediate certificates (2). Certificate Authorities use intermediate certificates to sign your server certificate and not the root certificate to increase the security. The idea is to revoke the intermediate certificates in case of a security breach, invalidating all its signed certificates with it. If that happened to the root certificate, it would affect all certificates in under it. Another interesting thing to note is that root certificates usually have much longer validity than other certificates. But this is probably for practical reasons as changing them is not as easy as switching the server certificate.

## The Practical Part
Now, if a company decides to roll out its own certificate authority in its service landscape, for example, it's usually not backed by a trusted authority. If you control the clients, you can install the company's root CA certificate. This is not a cost issue though. A big and important part for the certificate authorities is validating that you are a valid recipient of the signed certificate. This is done by challenging you with an HTTP request or requiring a predefined TXT record (DNS Challenge). This is not easily done in a private environment which often times leads to rolling out your own root certificate authority (which comes with challenges in itself)

## The issue
Finally, we come to the practical part. There were a couple of times when some teams would disable the certification verification process. When I hinted that all we need to resolve this issue is the root CA certificate, my team (of backend developer) was receptive about improving it but a bit clueless about the technical details that I was explaining. That's the reason I decided to make a blog post explaining it a bit more while showing a practical example.

## Demo
We'll deploy a Caddy with our self-signed server certificate and roll out the root CA certificate that was used to sign the server certificate. 

![blog-post-ca-cert-demo-overview.drawio.png](img/blog-post-ca-cert-demo-overview.drawio.png)

### Certificate Generation
To generate the necessary certificates, we can leverage the openSSL-CLI, mainly generating the root CA certificate and the server certificate. The signing is done by creating a Certificate Signing Request (.csr file) and finally signing it with the root CA private key.

There is a shorthand makefile target that can be run with make:
```bash
make certs/server.crt
```

### Roll-out
Now all clients require the root CA's certificate to trust the signed server certificate. This is rather difficult to give general advice for. In my experience, the setups differ so much because the combination of host system and app stack is gigantic that the best course is to read the effing manual. We can shed some light onto a _typical_ linux setup and a python client app.

Just to contrast this setup with a Java App, you would have to additionally introduce the root CA certificate to the [JVM trust store](https://docs.oracle.com/cd/E19830-01/819-4712/ablqw/index.html). So keep in mind that your setup may require additional or completely different steps.

Go and Python, however, fall back to the hosts' trusted certificates. On Linux systems, it is not that easy to determine the source of it. Check out [this](https://go.dev/src/crypto/x509/root_linux.go) interesting part of the go crypto library. 

#### Alpine/Linux
One of the most used linux distributions for container environments is alpine. Mostly because of its light footprint. We'll use this to showcase the certificate installation. Alpine uses the ca-certificates package to update the OS' trusted CA store. We simply have to copy our certificate in one of the expected sources for custom provided certificates (e.g. [/usr/local/share/ca-certificates](https://gitlab.alpinelinux.org/alpine/ca-certificates/-/blob/master/update-ca-certificates.8?ref_type=heads#L55)).

This is done in the Dockerfile with the following lines:
```dockerfile
COPY ./certs/rootCACert.crt /usr/local/share/ca-certificates/rootCACert.crt
RUN update-ca-certificates
```

Now the OS is set up to trust all certificates signed by our Foo CA. All we have to do is to check if our app stack requires additional configuration. As I am using python with the [requests](https://pypi.org/project/requests/) library, the docs point to setting an environment variable (_REQUESTS_CA_BUNDLE_) to the path of the OS' trust store file (_/etc/ssl/cert.pem_). Honestly, this was the hardest part because the doc was not working for me. I found out in a github issue that setting CURL_CA_BUNDLE instead would help (and it did). 

And that's it to trust our Foo CA without skipping or disabling TLS Certificate Verification, which poses a major threat to our security since we are prone to man-in-the-middle attacks. You can run the example with the provided docker-compose file I wrote:
```bash
docker compose up
```