# base
FROM python:3.11-alpine AS build-stage
COPY ./certs/rootCACert.crt /usr/local/share/ca-certificates/rootCACert.crt
RUN update-ca-certificates

RUN pip3 install requests

# app
RUN mkdir /app
WORKDIR /app
FROM build-stage AS app
COPY simple-request.py /app
