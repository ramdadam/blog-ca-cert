certs/%.key certs/%.crt:
	openssl genrsa -out certs/rootCAKey.pem 4096
	openssl req -x509 -sha256 -new -nodes -key certs/rootCAKey.pem -days 365 -out certs/rootCACert.crt -subj "/C=DE/ST=Berlin/L=Berlin/O=Foo/CN=Foo Root CA" -addext basicConstraints=critical,CA:TRUE,pathlen:1 -addext "keyUsage = keyCertSign"
	openssl genrsa -out certs/$*.key 4096
	openssl req -new -sha256 -key certs/$*.key -subj "/O=Foo/CN=https-server" -nodes -out certs/$*.csr
	openssl x509 -req -in certs/$*.csr -extfile tls.ext -CA certs/rootCACert.crt -CAkey certs/rootCAKey.pem -CAcreateserial -out certs/$*.crt -days 500 -sha256
